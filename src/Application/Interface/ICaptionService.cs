﻿using Atlas.Console.Captions.Domain.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atlas.Console.Captions.Application.Interface
{
    public interface ICaptionService
    {
        Task<IEnumerable<string>> AddCaptions(UpdateCaptionRequest caption);
    }
}
