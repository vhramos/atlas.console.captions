﻿using Atlas.Console.Captions.Domain.Entity;
using System.Threading.Tasks;

namespace Atlas.Console.Captions.Application.Interface
{
    public interface ITranslateService
    {
        Task<string> GetTranslation(Language langSource, Language langTarget, string text);
    }
}
