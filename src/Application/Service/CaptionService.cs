﻿using Atlas.Console.Captions.Application.Interface;
using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Domain.Interface;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Atlas.Console.Captions.Application.Service
{
    public class CaptionService : ICaptionService
    {
        private readonly ICaptionRepository _captionRepository;
        private readonly ITranslateService _translateService;
        private readonly ICaptionBehaviorRepository _captionBehaviorRepository;
        private readonly ICaptionPageBehaviorRepository _captionPageBehaviorRepository;
        private readonly ICaptionPageRepository _captionPageRepository;

        public CaptionService(ICaptionRepository captionRepository,
                              ITranslateService translateService,
                              ICaptionBehaviorRepository captionBehaviorRepository,
                              ICaptionPageBehaviorRepository captionPageBehaviorRepository,
                              ICaptionPageRepository captionPageRepository)
        {
            _captionRepository = captionRepository;
            _translateService = translateService;
            _captionBehaviorRepository = captionBehaviorRepository;
            _captionPageBehaviorRepository = captionPageBehaviorRepository;
            _captionPageRepository = captionPageRepository;
        }
        public async Task<IEnumerable<string>> AddCaptions(UpdateCaptionRequest caption)
        {
            var categories = await _captionRepository.GetAllCaptionCategories();
            var dctNewCaptionsLoggedArea = RemoveDuplicateCaptions(caption.CaptionsPtBr.LoggedArea, categories);
            var dctNewCaptionsUnlockedArea = RemoveDuplicateCaptions(caption.CaptionsPtBr.UnlockedArea, categories);
            var inseredCaptions = dctNewCaptionsLoggedArea.Select(c => c.Key).ToList();
            inseredCaptions.AddRange(dctNewCaptionsUnlockedArea.Select(c => c.Key).ToList());

            await AddCaptions(dctNewCaptionsLoggedArea, AppPage.LoggedArea, BehaviorPage.LoggedArea);
            await AddCaptions(dctNewCaptionsUnlockedArea, AppPage.UnlockedArea, BehaviorPage.UnlockedArea);

            return inseredCaptions;
        }

        private static Dictionary<string, string> RemoveDuplicateCaptions(Dictionary<string, string> dctCaptions,
                                                                          IEnumerable<string> existingCaptionsList)
        {
            return dctCaptions.Where(kvp => !existingCaptionsList.Contains(kvp.Key)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }
        private async Task AddCaptions(Dictionary<string, string> dctCaptions, AppPage appPage, BehaviorPage behaviorPage)
        {
            foreach (var kv in dctCaptions)
            {
                var titleEn = await _translateService.GetTranslation(Language.Portuguese, Language.English, kv.Value);
                var titleEs = await _translateService.GetTranslation(Language.Portuguese, Language.Spanish, kv.Value);

                var cptId = await _captionRepository.Add(new Caption()
                {
                    Category = kv.Key,
                    Title = titleEn
                });
                var cptBehIdPt = await _captionBehaviorRepository.Add(new CaptionBehavior()
                {
                    CaptionId = cptId,
                    LanguageId = (int)Language.Portuguese,
                    TitleTranslated = kv.Value
                });

                var cptBehIdEs = await _captionBehaviorRepository.Add(new CaptionBehavior()
                {
                    CaptionId = cptId,
                    LanguageId = (int)Language.Spanish,
                    TitleTranslated = titleEs
                });

                await _captionPageBehaviorRepository.Add(new CaptionPageBehavior()
                {
                    CaptionBehaviorId = cptBehIdPt,
                    BehaviorPagesId = (int)behaviorPage
                });

                await _captionPageBehaviorRepository.Add(new CaptionPageBehavior()
                {
                    CaptionBehaviorId = cptBehIdEs,
                    BehaviorPagesId = (int)behaviorPage
                });
                await _captionPageRepository.Add(new CaptionPage()
                {
                    AppPageId = (int)appPage,
                    CaptionId = cptId
                });


            }
        }
    }
}
