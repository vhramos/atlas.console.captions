﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon;
using Amazon.Translate;
using Amazon.Translate.Model;
using Atlas.Console.Captions.Application.Interface;
using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Infra.Util;

namespace Atlas.Console.Captions.Application.Service
{
    public class TranslateService : ITranslateService
    {
        private readonly AmazonTranslateClient _amazonTranslateClient;
        public TranslateService()
        {
            var accessKeyId = "AKIAJUIRAI6TO6DESNPA";
            var secretKey = "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";
            _amazonTranslateClient = new AmazonTranslateClient(accessKeyId, secretKey, RegionEndpoint.USWest2);
        }
        public async Task<string> GetTranslation(Language langSource,
                                                 Language langTarget,
                                                 string text)
        {
            var translate = await _amazonTranslateClient.TranslateTextAsync(new TranslateTextRequest()
            {
                SourceLanguageCode = langSource.ToDescription(),
                TargetLanguageCode = langTarget.ToDescription(),
                Text = text
            });
            return translate.TranslatedText;
        }

    }
}
