﻿namespace Atlas.Console.Captions.Domain.Entity
{
    public enum AppPage
    {
        UnlockedArea = 6,
        LoggedArea = 7
    }
}
