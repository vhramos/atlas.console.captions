﻿using System.ComponentModel;

namespace Atlas.Console.Captions.Domain.Entity
{
    public enum AtlasStatus
    {
        [Description("ACTIVE")]
        Active
    }
}
