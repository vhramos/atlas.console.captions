﻿namespace Atlas.Console.Captions.Domain.Entity
{
    public enum BehaviorPage
    {
        UnlockedArea = 1,
        LoggedArea = 2
    }
}
