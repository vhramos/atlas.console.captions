﻿using System;
using Atlas.Console.Captions.Infra.Util;
using Atlas.Data.Dapper.Configuration;

namespace Atlas.Console.Captions.Domain.Entity
{
    [TableConfiguration("CAPTION")]
    public class Caption
    {
        public Caption()
        {
            Status = AtlasStatus.Active.ToDescription();
            StatusDate = DateTime.UtcNow;
        }
        [AttributeConfiguration("CPT_ID", true)]
        public int Id { get; set; }
        [AttributeConfiguration("CPT_CATEGORY")]
        public string Category { get; set; }
        [AttributeConfiguration("CPT_STATUS")]
        public string Status { get; set; }
        [AttributeConfiguration("CPT_STATUS_DATE")]
        public DateTime StatusDate { get; set; }
        [AttributeConfiguration("CPT_TITLE")]
        public string Title { get; set; }
    }
}
