﻿using Atlas.Data.Dapper.Configuration;
using System;
using Atlas.Console.Captions.Infra.Util;

namespace Atlas.Console.Captions.Domain.Entity
{
    [TableConfiguration("CAPTION_BEHAVIOR")]
    public class CaptionBehavior
    {
        public CaptionBehavior()
        {
            StatusDate = DateTime.UtcNow;
            Status = AtlasStatus.Active.ToDescription();
        }
        [AttributeConfiguration("CPTBEH_ID", true)]
        public int CaptionBehaviorId { get; set; }
        [AttributeConfiguration("CPT_ID")]
        public int CaptionId { get; set; }
        [AttributeConfiguration("LANGUAGE_ID")]
        public int LanguageId { get; set; }
        [AttributeConfiguration("CPTBEH_TRANSLATE_TITLE")]
        public string TitleTranslated { get; set; }
        [AttributeConfiguration("CPTBEH_STATUS")]
        public string Status { get; set; }
        [AttributeConfiguration("CPTBEH_STATUS_DATE")]
        public DateTime StatusDate { get; set; }
    }
}
