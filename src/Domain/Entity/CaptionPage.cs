﻿using Atlas.Data.Dapper.Configuration;
using System;
using Atlas.Console.Captions.Infra.Util;

namespace Atlas.Console.Captions.Domain.Entity
{
    [TableConfiguration("CAPTION_PAGES")]
    public class CaptionPage
    {
        public CaptionPage()
        {
            Status = AtlasStatus.Active.ToDescription();
            StatusDate = DateTime.UtcNow;
        }
        [AttributeConfiguration("CPTAPPP_ID", true)]
        public int CaptionPageId { get; set; }
        [AttributeConfiguration("CPTAPPP_STATUS")]
        public string Status { get; set; }
        [AttributeConfiguration("CPTAPPP_STATUS_DATE")]
        public DateTime StatusDate { get; set; }
        [AttributeConfiguration("APPP_ID")]
        public int AppPageId { get; set; }
        [AttributeConfiguration("CPT_ID")]
        public int CaptionId { get; set; }
    }
}
