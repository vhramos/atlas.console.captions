﻿using Atlas.Console.Captions.Infra.Util;
using Atlas.Data.Dapper.Configuration;
using System;

namespace Atlas.Console.Captions.Domain.Entity
{
    [TableConfiguration("CAPTION_PAGE_BEHAVIOR")]
    public class CaptionPageBehavior
    {
        public CaptionPageBehavior()
        {
            Status = AtlasStatus.Active.ToDescription();
            StatusDate = DateTime.UtcNow;
        }
        [AttributeConfiguration("CPTBEHAPPP_ID", true)]
        public int CaptionPageBehaviorId { get; set; }
        [AttributeConfiguration("CPTBEH_ID")]
        public int CaptionBehaviorId { get; set; }
        [AttributeConfiguration("CPTBEHAPPP__STATUS")]
        public string Status { get; set; }
        [AttributeConfiguration("CPTBEHAPPP_STATUS_DATE")]
        public DateTime StatusDate { get; set; }
        [AttributeConfiguration("APPPB_ID")]
        public int BehaviorPagesId { get; set; }
    }
}
