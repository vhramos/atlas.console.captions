﻿using System.ComponentModel;

namespace Atlas.Console.Captions.Domain.Entity
{
    public enum Language
    {
        [Description("pt")]
        Portuguese = 406,
        [Description("en")]
        English = 999,
        [Description("es")]
        Spanish = 309
    }
}
