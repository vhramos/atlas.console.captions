﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Atlas.Console.Captions.Domain.Entity
{
    public class QuantumCaptions
    {
        [JsonProperty("unlocked-area")]
        public Dictionary<string, string> UnlockedArea { get; set; }
        [JsonProperty("logged-area")]
        public Dictionary<string, string> LoggedArea { get; set; }
    }
}
