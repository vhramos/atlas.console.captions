﻿using Newtonsoft.Json;

namespace Atlas.Console.Captions.Domain.Entity
{
    public class UpdateCaptionRequest
    {
        [JsonProperty("quantum")]
        public QuantumCaptions CaptionsPtBr { get; set; }
    }
}
