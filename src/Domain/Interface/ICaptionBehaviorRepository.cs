﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.Console.Captions.Domain.Entity;
using Atlas.Data.Dapper.Interface;

namespace Atlas.Console.Captions.Domain.Interface
{
    public interface ICaptionBehaviorRepository : IBaseDapperRepository<CaptionBehavior>
    {

    }
}
