﻿using Atlas.Console.Captions.Domain.Entity;
using Atlas.Data.Dapper.Interface;

namespace Atlas.Console.Captions.Domain.Interface
{
    public interface ICaptionPageRepository : IBaseDapperRepository<CaptionPage>
    {

    }
}
