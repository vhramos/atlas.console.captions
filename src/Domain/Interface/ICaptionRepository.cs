﻿using Atlas.Console.Captions.Domain.Entity;
using Atlas.Data.Dapper.Interface;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atlas.Console.Captions.Domain.Interface
{
    public interface ICaptionRepository : IBaseDapperRepository<Caption>
    {
        Task<Caption> GetCaptionByKey(string key);
        Task<bool> CaptionExists(string key);
        Task<IEnumerable<string>> GetAllCaptionCategories();
    }
}
