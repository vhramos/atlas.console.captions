﻿using Atlas.Console.Captions.Application.Interface;
using Atlas.Console.Captions.Application.Service;
using Microsoft.Extensions.DependencyInjection;
using System;
using Atlas.Console.Captions.Domain.Interface;
using Atlas.Console.Captions.Infra.Repository;
using Atlas.Data.Dapper.Configuration;

namespace Atlas.Console.Captions.Infra.IoC
{
    public class DependencyInjector
    {
        private static IServiceProvider ServiceProvider { get; set; }
        private static IServiceCollection Services { get; set; }
        public static T GetService<T>()
        {
            ServiceProvider = ServiceProvider ?? Services.BuildServiceProvider();
            return ServiceProvider.GetService<T>();
        }
        public static IServiceCollection RegisterServices()
        {
            Services = new ServiceCollection();
            Services.AddSingleton<ITranslateService, TranslateService>();
            Services.AddScoped<ICaptionService, CaptionService>();
            Services.AddScoped<ICaptionRepository, CaptionRepository>();
            Services.AddScoped<ICaptionBehaviorRepository, CaptionBehaviorRepository>();
            Services.AddScoped<ICaptionPageBehaviorRepository, CaptionPageBehaviorRepository>();
            Services.AddScoped<ICaptionPageRepository, CaptionPageRepository>();

            Services.AddDapper();
            return Services;
        }
    }
}
