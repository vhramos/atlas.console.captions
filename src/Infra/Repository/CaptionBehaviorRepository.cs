﻿using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Domain.Interface;
using Atlas.Data.Dapper.Interface;
using Atlas.Data.Dapper.Repository;

namespace Atlas.Console.Captions.Infra.Repository
{
    public class CaptionBehaviorRepository : BaseDapperRepository<CaptionBehavior>, ICaptionBehaviorRepository
    {
        public CaptionBehaviorRepository(IDapperUoW uow) : base(uow)
        {
        }
    }
}
