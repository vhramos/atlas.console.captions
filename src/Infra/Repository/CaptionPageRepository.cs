﻿using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Domain.Interface;
using Atlas.Data.Dapper.Interface;
using Atlas.Data.Dapper.Repository;

namespace Atlas.Console.Captions.Infra.Repository
{
    public class CaptionPageRepository : BaseDapperRepository<CaptionPage>, ICaptionPageRepository
    {
        public CaptionPageRepository(IDapperUoW uow) : base(uow)
        {
        }
    }
}
