﻿using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Domain.Interface;
using Atlas.Data.Dapper.Interface;
using Atlas.Data.Dapper.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Atlas.Console.Captions.Infra.Repository
{
    public class CaptionRepository : BaseDapperRepository<Caption>, ICaptionRepository
    {
        public CaptionRepository(IDapperUoW uow) : base(uow)
        {
        }
        public async Task<Caption> GetCaptionByKey(string key)
        {
            var param = new
            {
                key
            };
            return await QueryFirstOrDefaultAsync<Caption>(@"SELECT CPT_ID Id,
                                                                    CPT_CATEGORY Category,
                                                                    CPT_TITLE Title
                                                                    FROM CAPTION
                                                                    WHERE CPT_CATEGORY =: key", param);
        }
        public async Task<bool> CaptionExists(string key)
        {
            var caption = await GetCaptionByKey(key);
            return caption != null && !string.IsNullOrEmpty(caption.Category);
        }
        public async Task<IEnumerable<string>> GetAllCaptionCategories()
        {
            return await QueryAsync<string>(@"SELECT CPT_CATEGORY FROM CAPTION");
        }
    }
}
