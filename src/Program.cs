﻿using Atlas.Console.Captions.Application.Interface;
using Atlas.Console.Captions.Domain.Entity;
using Atlas.Console.Captions.Infra.IoC;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;

namespace Atlas.Console.Captions
{
    class Program
    {
        private static ICaptionService _captionService;
        static void Main(string[] args)
        {
            while (true)
            {
                try
                {
                    System.Console.WriteLine();
                    System.Console.WriteLine("##### Atlas.Console.Captions ####");
                    System.Console.WriteLine("##### ATENÇÃO: Captions.json deve estar no formato UTF8  ####");
                    System.Console.WriteLine("##### Opções : 1 - Upload de captions  ####");
                    System.Console.Write("##### Opção:");

                    var opcao = Convert.ToInt16(System.Console.ReadLine());
                    if (opcao == 1)
                    {
                        SetEnviromentVariables();
                        DependencyInjector.RegisterServices();
                        _captionService = DependencyInjector.GetService<ICaptionService>();
                        var updateCaptionRequest = JsonConvert.DeserializeObject<UpdateCaptionRequest>(File.ReadAllText(@"Json\Captions.json", Encoding.UTF8));
                        var task = _captionService.AddCaptions(updateCaptionRequest);
                        while (!task.IsCompleted)
                        {
                            System.Console.Write("#");
                            Thread.Sleep(500);
                        }

                        System.Console.WriteLine();
                        System.Console.WriteLine("Inserted Captions:");
                        System.Console.WriteLine(string.Join(Environment.NewLine, task.Result));
                        System.Console.ReadKey();
                    }
                }
                catch (Exception error)
                {
                    System.Console.WriteLine(error.Message);
                }
            }
        }
        private static void SetEnviromentVariables()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();

            Environment.SetEnvironmentVariable("SecretsManager_AwsSecret", configuration["SecretsManager_AwsSecret"]);
            Environment.SetEnvironmentVariable("SecretsManager_Name", configuration["SecretsManager_Name"]);
            Environment.SetEnvironmentVariable("SecretsManager_AwsKey", configuration["SecretsManager_AwsKey"]);
            Environment.SetEnvironmentVariable("SecretsManager_AwsRegion", configuration["SecretsManager_AwsRegion"]);
        }
    }
}
